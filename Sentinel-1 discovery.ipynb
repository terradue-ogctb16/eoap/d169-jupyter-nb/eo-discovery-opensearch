{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Etna volcano discovery\n",
    "\n",
    "## Sentinel-1 SLC "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Authentication information](#Authentication-information) \n",
    "\n",
    "[Collection correlate](#Collection-correlate)\n",
    "\n",
    "This section demoes the Collection.correlate verb providing correlated search where an operation is made of two functions: \n",
    "\n",
    "- the first function can manipulate the filter key/values\n",
    "- the second function can manipulate the filtered collection results\n",
    "\n",
    "Default Operations are implemented in the file operations.py\n",
    "\n",
    "Operations can be implemented on-the-fly\n",
    "\n",
    "The demos included are:\n",
    "\n",
    "- Identify an interferometric pair of Sentinel-1 SLC products\n",
    "- Identify an interferometric stack\n",
    "- Provide the intersection percent with relation to an area of interest\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import getpass\n",
    "from shapely.geometry import box\n",
    "from catalogc import Collection\n",
    "import gdal\n",
    "from shapely.ops import cascaded_union\n",
    "from shapely.wkt import loads\n",
    "import numpy as np\n",
    "from ipyleaflet import Map, Polygon\n",
    "from sidecar import Sidecar"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Authentication information"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdin",
     "output_type": "stream",
     "text": [
      "Provide your Terradue username fbrito\n"
     ]
    }
   ],
   "source": [
    "username = input('Provide your Terradue username')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdin",
     "output_type": "stream",
     "text": [
      "And your Terradue API key: ·········································································\n"
     ]
    }
   ],
   "source": [
    "api_key = getpass.getpass('And your Terradue API key:')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Describe the Sentinel-1 collection"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "osd = 'https://catalog.terradue.com/sentinel1/description'\n",
    "\n",
    "collection = Collection(osd, username, api_key)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "dict_keys(['count', 'startPage', 'startIndex', 'q', 'lang', 'update', 'do', 'from', 'start', 'stop', 'bbox', 'geom', 'uid', 'rel', 'trel', 'cat', 'ext', 'source', 'pt', 'psn', 'psi', 'isn', 'st', 'pl', 'od', 'ot', 'res', 'title', 'pi', 'track', 'swath', 'lc', 'dcg', 'cc', 'vs'])"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "collection.description.keys()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'title': 'A string identifying the product type',\n",
       " 'value': '{eop:productType?}',\n",
       " 'options': ['GRD', 'RAW', 'SLC', 'OCN']}"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "collection.description['pt']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['GRD', 'RAW', 'SLC', 'OCN']"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "collection.description['pt']['options']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load the land vectors from Natural Earth"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "land = gdal.OpenEx('ne_10m_admin_0_countries.gpkg')\n",
    "\n",
    "layer = land.GetLayer()\n",
    "featureCount = layer.GetFeatureCount()\n",
    "\n",
    "land_entries = [] \n",
    "\n",
    "for index, feature in enumerate(layer):\n",
    "\n",
    "    geom = feature.GetGeometryRef()\n",
    "    land_entries.append(loads(geom.ExportToWkt()))\n",
    "        \n",
    "land_union = cascaded_union(land_entries)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create a correlation operation combining the land coverage and the AOI intersection"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "class AoiLandAnalysisOperation:\n",
    "    \"\"\"\n",
    "    AoiLandAnalysisOperation\n",
    "    \"\"\"\n",
    "    def __init__(self):\n",
    "        pass\n",
    "    \n",
    "    @staticmethod\n",
    "    def get_intersection(row, aoi):\n",
    "\n",
    "        return (row['geometry'].intersection(aoi).area / aoi.area) * 100\n",
    "    \n",
    "    @staticmethod\n",
    "    def get_land_intersection(row, land):\n",
    "\n",
    "        return (row['geometry'].intersection(land).area / row['geometry'].area) * 100\n",
    "    \n",
    "    def pre_correlate(self, collection, operation, filters):\n",
    "\n",
    "        return filters\n",
    "\n",
    "    def correlate(self, collection, operation, dataset):\n",
    "\n",
    "        dataset['aoi_intersec'] = dataset.apply(lambda row: self.get_intersection(row, \n",
    "                                                                                  operation['aoi']), \n",
    "                                              axis=1)\n",
    "        \n",
    "        dataset['land_intersec'] = dataset.apply(lambda row: self.get_land_intersection(row, \n",
    "                                                                                       operation['land']), \n",
    "                                              axis=1)\n",
    "\n",
    "        dataset = dataset[dataset['land_intersec'] >= float(operation['land_threshold'])].reset_index(drop=True)\n",
    "        \n",
    "        return dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Query"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "operation = dict()\n",
    "operation['name'] = 'AoiLandAnalysisOperation'\n",
    "operation['aoi'] = loads('POINT(14.9934 37.7510)').buffer(0.2)\n",
    "operation['land'] = land_union\n",
    "operation['land_threshold'] = 30\n",
    "\n",
    "s1_collection = Collection(username=username, \n",
    "                           api_key=api_key, \n",
    "                           osd='https://catalog.terradue.com/sentinel1/description',\n",
    "                           fields='identifier,self,wkt,cat,title,orbitDirection,track')\n",
    "\n",
    "s1_collection.register_operation(AoiLandAnalysisOperation)\n",
    "\n",
    "reference_masters = s1_collection.correlate(operation=operation,\n",
    "                                            geom=loads('POINT(14.9934 37.7510)').buffer(0.2).wkt,\n",
    "                                            pt='SLC',\n",
    "                                            count=30,\n",
    "                                            start='2019-12-01T00:00:00Z',\n",
    "                                            stop='2020-01-01T23:59:59Z')\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [],
   "source": [
    "def to_map_polygon(geometry):\n",
    "    \n",
    "    return Polygon(locations=np.asarray([t[::-1] for t in list(geometry.exterior.coords)]).tolist(), \n",
    "            color=\"red\", \n",
    "            fill_color=\"green\")\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [],
   "source": [
    "m = Map(center=(37.7510, 14.9934), zoom=6)\n",
    "\n",
    "m.add_layer(to_map_polygon(loads('POINT(14.9934 37.7510)').buffer(0.2)));\n",
    "m.add_layer(to_map_polygon(reference_masters.iloc[1].geometry));\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [],
   "source": [
    "scm = Sidecar(title='Map')\n",
    "\n",
    "with scm:\n",
    "    display(m)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [],
   "source": [
    "sc = Sidecar(title='Catalog results')\n",
    "\n",
    "with sc:\n",
    "    display(reference_masters)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [],
   "source": [
    "# global layer_group \n",
    "\n",
    "# layer_group = LayerGroup(layers=())\n",
    "# m.add_layer(layer_group)\n",
    "\n",
    "# def f(x):\n",
    "#     layer_group.clear_layers()\n",
    "#     m.remove_layer(layer_group)\n",
    "        \n",
    "#     p = Polygon(locations=np.asarray([t[::-1] for t in list(list(s1_results.iloc[[x]]['wkt'])[0].exterior.coords)]).tolist(), color=\"red\", fill_color=\"green\")\n",
    "    \n",
    "#     d = {'identifier': list(s1_results.iloc[[x]]['identifier'])[0], \n",
    "#          'track' :list(s1_results.iloc[[x]]['track'])[0]}\n",
    "    \n",
    "#     html_value=\"\"\"\n",
    "#         <div>\n",
    "#         <ul class='list-group'>\n",
    "#             <li class='list-group-item'>{identifier}</li>\n",
    "#             <li class='list-group-item'>{track}</li>\n",
    "#         </ul></div>\"\"\".format(**d)\n",
    "    \n",
    "    \n",
    "#     html_widget_slave = HTML(\n",
    "#             value=html_value,\n",
    "#     placeholder='',\n",
    "#     description='',\n",
    "#     )\n",
    "    \n",
    "    \n",
    "    \n",
    "#     layer_group.add_layer(p)\n",
    "#     p.popup = html_widget_slave\n",
    "#     m.add_layer(layer_group)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\"><img alt=\"Creative Commons License\" style=\"border-width:0\" src=\"https://i.creativecommons.org/l/by-sa/4.0/88x31.png\" /></a><br />This work is licensed under a <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\">Creative Commons Attribution-ShareAlike 4.0 International License</a>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "kernel",
   "language": "python",
   "name": "kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
