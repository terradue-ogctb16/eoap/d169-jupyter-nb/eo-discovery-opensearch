FROM terradue/l1-binder:eb005c722abd

MAINTAINER Terradue S.r.l

USER ${NB_USER}

COPY . ${HOME}

RUN /opt/anaconda/bin/conda env create --name env-binder --file ${HOME}/environment.yml

ENV PREFIX /opt/anaconda/envs/env-binder

RUN ${PREFIX}/bin/python -m ipykernel install --name "Binder" && \
    ${PREFIX}/bin/jupyter nbextension enable --py --sys-prefix widgetsnbextension && \
    ${PREFIX}/bin/jupyter nbextension enable --py --sys-prefix ipyleaflet && \
    ${PREFIX}/bin/pip install sidecar

RUN test -f ${HOME}/postBuild && chmod 755 ${HOME}/postBuild && ${HOME}/postBuild || true

WORKDIR ${HOME}
